#include "DecoratorExample.hh"


// the constructor just builds the decorator
DecoratorExample::DecoratorExample(const std::string& prefix):
  //  m_deco(prefix + "decorator")
  m_deco("SV1_LLR"),
  m_deco_bH_pt("bH_pt"),
  m_deco_bH_DR("bH_DR")
{
}

// this call actually does the work on the jet
void DecoratorExample::decorate(const xAOD::Jet& jet) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = jet.btagging();
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }


  using xAOD::IParticle;
  std::vector<const IParticle*> ghostB;
  // std::vector<double> bH_pt;
  float singlebH_pt=-1,singlebH_DR=-1;

  const std::string labelB = "ConeExclBHadronsFinal";
  jet.getAssociatedObjects<IParticle>(labelB, ghostB);

  if ( ghostB.size()==1 ){

    const xAOD::TruthParticle * myB=(const xAOD::TruthParticle*)(ghostB.at(0));//was iB

    singlebH_pt=myB->pt();
    singlebH_DR=DR(myB,jet);
  }


  // Store something to the jet.
  //  m_deco(*btag) = std::log(jet.pt());
  //  m_deco_nbH(jet)= ghostB.size();

  m_deco(*btag)=btag->SV1_loglikelihoodratio();
  m_deco_bH_pt(*btag)=singlebH_pt;
  m_deco_bH_DR(*btag)=singlebH_DR;
}

float DecoratorExample::DR(const xAOD::TruthParticle *bH, const xAOD::Jet& jet) const{
  float Deta=bH->eta()-jet.eta();
  float tmp_Dphi=std::abs(bH->phi()-jet.phi());
  float Dphi=0.;

  if(tmp_Dphi>M_PI){
    Dphi=2*M_PI-tmp_Dphi;
  }
  else{
    Dphi=tmp_Dphi;
  }

  return std::sqrt(Deta*Deta+Dphi*Dphi);

}
