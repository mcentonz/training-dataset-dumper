#include "TrackDecorator.hh"
#include "xAODTruth/TruthParticle.h"
#include "InDetTrackTruthOriginDefs.h"

// the constructor just builds the decorator
TrackDecorator::TrackDecorator(const std::string& prefix):
  m_track_pt(prefix + "pt"),
  m_track_logpt(prefix + "logpt"),
  m_track_eta(prefix + "eta"),
  m_trk_origin(prefix + "origin"),
  m_trk_derivedorigin(prefix + "der_origin"),
  m_trk_tmp(prefix + "tmp"),
  m_trackTruthOriginTool("trackTruthOriginTool")
{
  if (!m_trackTruthOriginTool.initialize()) {
    throw std::logic_error("can't initialize trackTruthOriginTool");
  }
}

void TrackDecorator::decorate_track(const xAOD::TrackParticle &track) const {

  const xAOD::TruthParticle* truth = getTruth(track);

  int InDetTrackTruthOrigin = m_trackTruthOriginTool.getTrackOrigin( &track ); // inclusive origins
  int exclusive_track_label = get_track_label(InDetTrackTruthOrigin, truth);//InDetTrackTruthOrigin, truth);  // exclusive labels
  int derived_track_label = get_DerivedTrackLabel(InDetTrackTruthOrigin, truth);

  float TMP=-1;
  try {
    TMP = track.auxdata< float >("truthMatchProbability");
  } catch(...) {
    std::cout << " getTrackOrigin, no truthMatchProbability " << std::endl;
  };


  m_track_pt(track)=track.pt();
  m_track_logpt(track)=std::log(track.pt());
  m_track_eta(track)=track.eta();
  m_trk_origin(track)=exclusive_track_label;
  m_trk_derivedorigin(track)=derived_track_label;
  m_trk_tmp(track)=TMP;
}


const xAOD::TruthParticle* TrackDecorator::getTruth( const xAOD::TrackParticle &track ) const {

  // create a pointer to a truth particle which will correspond to this track
  const xAOD::TruthParticle* linkedTruthParticle = nullptr;

  // if the track doesnt't have a valid truth link, skip to the next track
  // in practice, all tracks seem to have a truth link, but we need to also
  // check whether it's valid
  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  if ( !(track.isAvailable<TruthLink>("truthParticleLink")) ) { 
    return nullptr;
  }

  // retrieve the link and check its validity
  const TruthLink &link = track.auxdata<TruthLink>("truthParticleLink");

  // a missing or invalid link implies truth particle has been dropped from 
  // the truth record at some stage - probably it was from pilup which by
  // default we don't store truth information for
  if(!link or !link.isValid()) {
    return nullptr;
  }

  // seems safe to access and return the linked truth particle
  linkedTruthParticle = (*link);
  return linkedTruthParticle;
}

int TrackDecorator::get_track_label(int origin, const xAOD::TruthParticle *truth) const {
  
//    The origin from the InDetTrackTruthOriginTool has inclusive categories.
//    Here we augment the origin to build exclusive categories.
  

  int exclusive_track_label = -1;


  if ( InDet::TrkOrigin::isPileup(origin) or !truth ) {
    exclusive_track_label = 0; // PU / no truth link
  } 
  else { 
    // truth link availible, let's be more specific
    if ( InDet::TrkOrigin::isSecondary(origin) ) {
      exclusive_track_label = 1; // good Geant
    }
    else if ( not truth->isHadron() ) {
      exclusive_track_label = 3; // good lepton
    }
    else {
      exclusive_track_label = 5; // good hadron
    }

    // modify the above categories
    if ( InDet::TrkOrigin::isFake(origin) ) {
      exclusive_track_label += 1; // make it fake
    }
    if ( InDet::TrkOrigin::isFromD(origin) ) {
      exclusive_track_label += 10; // make it From D
    }
    if ( InDet::TrkOrigin::isFromB(origin) ) {
      exclusive_track_label += 100; // make it From B
    }
  }
    
  return exclusive_track_label;
}

int TrackDecorator::get_DerivedTrackLabel(int origin, const xAOD::TruthParticle* truth) const {
  /*
    The origin from the InDetTrackTruthOriginTool has inclusive categories.
    Here we augment the origin to build exclusive categories.
  */

  int exclusive_track_label = get_track_label(origin, truth);

  int Derived_origin = -1;

  if(exclusive_track_label==0)  Derived_origin=0;
  if(exclusive_track_label==2 || exclusive_track_label==4 || exclusive_track_label==6)  Derived_origin=1;
  if(exclusive_track_label==12 || exclusive_track_label==14 || exclusive_track_label==16 || exclusive_track_label==102 || exclusive_track_label==104 || exclusive_track_label==106 || exclusive_track_label==112 || exclusive_track_label==114 || exclusive_track_label==116)  Derived_origin=2;
  if(exclusive_track_label==1 || exclusive_track_label==11 || exclusive_track_label==101 || exclusive_track_label==111)  Derived_origin=3;
  if(exclusive_track_label==3 || exclusive_track_label==5)  Derived_origin=4;
  if(exclusive_track_label==13 || exclusive_track_label==103 || exclusive_track_label==113 || exclusive_track_label==15 || exclusive_track_label==105 || exclusive_track_label==115) Derived_origin=5;
    
  return Derived_origin;
}
