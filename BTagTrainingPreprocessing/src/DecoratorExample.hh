#ifndef DECORATOR_EXAMPLE_HH
#define DECORATOR_EXAMPLE_HH

#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class DecoratorExample
{
public:
  DecoratorExample(const std::string& decorator_prefix = "example_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;
  float DR(const xAOD::TruthParticle *bH, const xAOD::Jet& jet) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  SG::AuxElement::Decorator<float> m_deco;
  //  SG::AuxElement::Decorator<int> m_deco_nbH;
  SG::AuxElement::Decorator<float> m_deco_bH_pt;
  SG::AuxElement::Decorator<float> m_deco_bH_DR;

};

#endif
