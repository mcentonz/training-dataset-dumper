#ifndef TRACK_DECORATOR_HH
#define TRACK_DECORATOR_HH


#include "xAODJet/Jet.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "InDetTrackTruthOriginTool.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

//namespace InDet { class IInDetTrackSelectionTool; }

class TrackDecorator
{
public:
  TrackDecorator(const std::string& decorator_prefix = "trk_");

  // this is the function that actually does the decoration
  void decorate_track(const xAOD::TrackParticle &track) const;
  const xAOD::TruthParticle* getTruth( const xAOD::TrackParticle &track ) const;
  int get_track_label(int origin, const xAOD::TruthParticle* truth) const;
  int get_DerivedTrackLabel(int origin, const xAOD::TruthParticle* truth) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  SG::AuxElement::Decorator<float> m_track_pt;
  SG::AuxElement::Decorator<float> m_track_logpt;
  SG::AuxElement::Decorator<float> m_track_eta;
  SG::AuxElement::Decorator<int> m_trk_origin;
  SG::AuxElement::Decorator<int>  m_trk_derivedorigin;
  SG::AuxElement::Decorator<float> m_trk_tmp;
  InDet::InDetTrackTruthOriginTool m_trackTruthOriginTool;

};

#endif
