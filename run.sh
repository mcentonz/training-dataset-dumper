#!/usr/bin/env bash


while getopts "d:" opt; do
    case "${opt}" in
        d)
            d=${OPTARG}
	    (( d==0 || d==1 ))
#	    echo d
            ;;
	*)
            echo "Incorrect options provided"
            exit 1
            ;;
    esac
done


if [ $d -eq 0 ] ; then

DIR=/eos/user/m/mcentonz/File/FTPF/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_e5984_s3126_r10201_r10210_p4062/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_e5984_s3126_r10201_r10210_p4062/
echo $d: run on ttbar
echo $DIR

for i in {01..14}
do
	echo ${i}
	FILE=DAOD_FTAG1.20480638._0000${i}.pool.root.1
	INPUT_FILE=${DIR}${FILE}

	if [[ ! -f $INPUT_FILE ]] ; then
	    echo "no file"
	fi

	echo $(dirname ${BASH_SOURCE[0]})/${CFG_FILE} $INPUT_FILE
	
	# run the script
	CFG_FILE=configs/single-b-tag/EMPFlow.json

	dump-single-btag -t -c ../training-dataset-dumper/configs/single-b-tag/EMPFlow.json $INPUT_FILE -o output_ttbar_EMPFlow_${i}.h5
done

fi

if [ $d -eq 1 ] ; then

DIR=/eos/user/m/mcentonz/File/FTPF/mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG1.e5362_e5984_s3126_r10724_r10726_p3985/
echo $d: run on Zprime
echo $DIR

for i in {10..32}
do
	FILE=DAOD_FTAG1.20123788._0000${i}.pool.root.1
	INPUT_FILE=${DIR}${FILE}

	if [[ ! -f $INPUT_FILE ]] ; then
		echo "no file"
	fi

	echo $(dirname ${BASH_SOURCE[0]})/${CFG_FILE} $INPUT_FILE

        # run the script
        CFG_FILE=configs/single-b-tag/EMPFlow.json

        dump-single-btag -t -c ../training-dataset-dumper/configs/single-b-tag/EMPFlow.json $INPUT_FILE -o output_Zprime_${i}.h5
done

fi

